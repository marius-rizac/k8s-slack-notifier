.PHONY: save build run

version = $(shell date +'%Y%m%d%H%M%S')

save:
	pip freeze > requirements.txt

build:
	docker build -t registry.gitlab.com/marius-rizac/k8s-slack-notifier:latest -f docker/Dockerfile .

run:
	-docker stop python-slack-notifier
	docker run --rm \
		-e SLACK_TOKEN='' \
		-e SLACK_CHANNEL='' \
		-e SLACK_BOT_NAME='k8s-pods' \
		-e FLASK_DEBUG='True' \
		-p 5000:5000 \
		--name=python-slack-notifier \
		-t registry.gitlab.com/marius-rizac/k8s-slack-notifier:latest
