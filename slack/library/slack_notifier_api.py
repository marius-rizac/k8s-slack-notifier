
class SlackApi(object):
    def __init__(self, settings, client):
        self.settings = settings
        self.slack_client = client

    def notify_pod_launch(self, pod_id, environment):
        message = ":arrow_forward: Pod *{0}* started ({1})".format(pod_id, environment)
        self._send_message(message)

    def notify_pod_terminate(self, pod_id, environment):
        message = ":eject: Pod *{0}* deleted ({1})".format(pod_id, environment)
        self._send_message(message)

    def notify_404_page(self, path):
        message = ":interrobang: Page *{0}* is not available".format(path)
        self._send_message(message)

    def _send_message(self, message):
        self.slack_client.api_call(
            'chat.postMessage',
            channel=self.settings.SLACK_CHANNEL,
            text=message,
            username=self.settings.SLACK_BOT_NAME,
            icon_emoji=':k8s:'
        )

    def _create_payload(self, message):
        return {
            'channel': self.settings.SLACK_CHANNEL,
            'username': self.settings.SLACK_BOT_NAME,
            'text': message,
        }
