from .settings import Settings
from .slack_notifier_api import SlackApi
from slackclient import SlackClient


class SlackFactory:
    @staticmethod
    def create():
        settings = Settings()
        client = SlackClient(settings.SLACK_TOKEN)

        return SlackApi(settings, client)
