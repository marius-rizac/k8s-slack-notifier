import os


class Settings(object):
    def __init__(self):
        self.SLACK_TOKEN = os.getenv('SLACK_TOKEN')
        self.SLACK_CHANNEL = os.getenv('SLACK_CHANNEL')
        self.SLACK_BOT_NAME = os.getenv('SLACK_BOT_NAME', 'k8s-bot')
        self.FLASK_DEBUG = os.getenv('FLASK_DEBUG', False)
