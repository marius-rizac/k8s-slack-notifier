#!/usr/bin/env python

import os
from flask import Flask, jsonify, send_from_directory
from library import SlackFactory

app = Flask(__name__)
app.config.from_object(__name__)

api = SlackFactory.create()


@app.route('/')
def index():
    return jsonify({'message': 'OK'})


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/start/<pod_name>/', defaults={'environment': 'default'})
@app.route('/start/<pod_name>/<environment>')
def start(pod_name, environment):
    api.notify_pod_launch(pod_name, environment)
    return jsonify({'message': 'starting %s' % pod_name})


@app.route('/stop/<pod_name>/', defaults={'environment': 'default'})
@app.route('/stop/<pod_name>/<environment>')
def stop(pod_name, environment):
    api.notify_pod_terminate(pod_name, environment)
    return jsonify({'message': 'stopping %s' % pod_name})


@app.route('/<path:path>')
def catch_all(path):
    api.notify_404_page(path)
    return jsonify({'message': 'Path %s is not available' % path})


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=bool(os.getenv('FLASK_DEBUG', False))
    )
