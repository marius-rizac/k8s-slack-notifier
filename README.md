## Build docker image

```bash
docker build -t registry.gitlab.com/marius-rizac/k8s-slack-notifier:latest -f docker/Dockerfile .
```

## Run locally

```bash
docker run --rm \
		-e SLACK_TOKEN='{add-here-slack-token}' \
		-e SLACK_CHANNEL='{add-here-slack-channel}' \
		-e SLACK_BOT_NAME='{add-here-stack-bot-name}' \
		-p 5000:5000 \
		--name=python-slack-notifier \
		-t registry.gitlab.com/marius-rizac/k8s-slack-notifier:latest
```

## Deploy on kubernetes


## Configure containers lifecycles

```yaml
lifecycle:
    postStart:
      exec:
        command:
          - "/bin/bash"
          - "-c"
          - 'curl -s -X GET --max-time 3000 http://slack-notifier.notifications.svc.cluster.local/start/${HOSTNAME}/notifier >&1; exit 0'
    preStop:
      exec:
        command:
          - "/bin/bash"
          - "-c"
          - 'curl -s -X GET --max-time 3000 http://slack-notifier.notifications.svc.cluster.local/stop/${HOSTNAME}/notifier >&1; exit 0'
```
